package com.rizki.trash.Handler;

import com.rizki.trash.GameMain;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

public class InputHandler implements GestureDetector.GestureListener {

    private GameMain game;
    private Sprite sprite;

    public InputHandler(GameMain game, Sprite sprite) {
        this.game = game;
        this.sprite = sprite;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        float posX = sprite.getX();
        float movement = Settings.MOVEMENT_SPEED * deltaX;

        if (posX + movement > 0 && posX + movement < game.width - sprite.getWidth()) {
            sprite.translateX(movement);
        }

//        Gdx.app.debug("InputHandler", "pan x " + x);
//        Gdx.app.debug("InputHandler", "pan deltaX " + deltaX);
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}

package com.rizki.trash.Handler;

import com.rizki.trash.pref.Assets;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ScoreHandler {

    private GlyphLayout glyphScore = new GlyphLayout();
    private String scoreText = "Score: 0";

    private float x = 0;
    private float y = 0;

    public ScoreHandler() {
        glyphScore.setText(Assets.gameFont, scoreText);
        updatePosition();
    }

    public void draw(SpriteBatch batch) {
        Assets.gameFont.draw(batch, scoreText, x, y);
    }

    public void updatePosition() {
        x = 100;
        y = Gdx.graphics.getHeight() - 300;
    }

    public void updateScore(int score) {
        scoreText = "Score: " + score;
        glyphScore.setText(Assets.gameFont, scoreText);

        updatePosition();
    }

    public void dispose() {
        Assets.gameFont.dispose();
    }
}

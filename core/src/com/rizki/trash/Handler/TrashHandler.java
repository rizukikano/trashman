package com.rizki.trash.Handler;

import com.badlogic.gdx.graphics.Texture;
import com.rizki.trash.GameMain;
import com.rizki.trash.Interface.ObjectListener;
import com.rizki.trash.Object.Trash;
import com.rizki.trash.parents.ObjectHandler;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TrashHandler extends ObjectHandler {

    private List<Trash> trashes;
    private float scale = 0.6f;

    public TrashHandler(GameMain game, Sprite sprite, Texture... textures) {
        super(game, sprite, textures);

        initialize();
    }

    private void initialize() {
        trashes = new ArrayList<Trash>();

        Trash trash = new Trash(textures, new Random().
                nextInt(textures.length));
        trash.setScale(scale);
        trash.setY(game.height);
        trashes.add(trash);
    }

    @Override
    public void setObjectListener(ObjectListener objectListener) {
        super.setObjectListener(objectListener);
    }

    public void add(int index) {
        Trash trash = new Trash(textures, new Random().
                nextInt(textures.length));
        trash.setPosIndex(index);
        trash.setScale(scale);
        trash.setY(game.height);
        trashes.add(trash);
    }

    public void draw(int index) {
        int i = 0;

        while (i < trashes.size()) {
            Trash trash = trashes.get(i);

            trash.setX(getHorizPosition(0, trash.getPosIndex() == -1 ?
                    index : trash.getPosIndex()));
            trash.draw(game.batch);
            trash.translateY(Settings.FALLING_SPEED);

            if (sprite != null) {

                if (trash.getY() >= game.height) {
                    trashes.remove(i);
                    objectListener.onObjectPassed();
                    continue;
                }

                if (trash.getBoundingRectangle().overlaps(sprite.getBoundingRectangle())) {
                    trashes.remove(i);
                    objectListener.onObjectCollision(trash.isReduce());
                }
            }

            i++;
        }
    }
}

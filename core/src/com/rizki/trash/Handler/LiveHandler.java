package com.rizki.trash.Handler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.rizki.trash.pref.Assets;

public class LiveHandler {
    private GlyphLayout glyphScore = new GlyphLayout();
    private String livesText = "Lives: 3";

    private float x = 0;
    private float y = 0;

    public LiveHandler() {
        glyphScore.setText(Assets.gameFont, livesText);
        updatePosition();
    }

    public void draw(SpriteBatch batch) {
        Assets.gameFont.draw(batch, livesText, x, y);
    }

    public void updatePosition() {
        x = 100;
        y = Gdx.graphics.getHeight() - 150;
    }

    public void updateLives(int score) {
        livesText = "Lives: " + score;
        glyphScore.setText(Assets.gameFont, livesText);

        updatePosition();
    }

    public void dispose() {
        Assets.gameFont.dispose();
    }
}

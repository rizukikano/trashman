package com.rizki.trash.Scene;

import com.rizki.trash.GameMain;
import com.rizki.trash.Handler.InputHandler;
import com.rizki.trash.Handler.LiveHandler;
import com.rizki.trash.Handler.TrashHandler;
import com.rizki.trash.Handler.ScoreHandler;
import com.rizki.trash.Interface.AdsServices;
import com.rizki.trash.Interface.ObjectListener;
import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.Object.Player;
import com.rizki.trash.Object.Score;
import com.rizki.trash.pref.Assets;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.input.GestureDetector;

import java.util.Random;

public class PlayScene extends ScreenAdapter {

    private GameMain game;
    private PlayServices playServices;
    private AdsServices adsServices;
    private Integer lives;
    private Integer score;

    private LiveHandler liveHandler;
    private ScoreHandler scoreHandler;
    private InputHandler inputHandler;
    private TrashHandler trashHandler;
    private Sprite sprite;

    private int SAW_INDEX = 0;

    private float timer;
    private int threshold[] = { 5, 25, 100, 200};

    public PlayScene(GameMain game, PlayServices playServices, AdsServices adsServices) {
        this.game = game;
        this.playServices = playServices;
        this.adsServices = adsServices;

        lives = 3;
        score = 0;
        sprite = new Player(0.1f, Assets.trash);

        sprite.setScale(0.7f);
        sprite.setPosition(game.width / 2 - sprite.getWidth() / 2,
                100);

        liveHandler = new LiveHandler();
        liveHandler.updateLives(lives);

        scoreHandler = new ScoreHandler();
        scoreHandler.updateScore(score);

        inputHandler = new InputHandler(game, sprite);
        Gdx.input.setInputProcessor(new GestureDetector(inputHandler));

        trashHandler = new TrashHandler(game, sprite, Assets.trashes);

        SAW_INDEX = new Random().nextInt(3);
    }

    private void draw() {
        Gdx.gl.glClearColor(((float) 83/255), ((float) 104/255), ((float) 211/255), 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        game.batch.draw(Assets.gameBackground,game.getCenter(Assets.gameBackground),0);
        timer += Gdx.graphics.getDeltaTime();

        liveHandler.draw(game.batch);
        scoreHandler.draw(game.batch);
        sprite.draw(game.batch);

        if (timer >= Settings.SPAWN_TIME) {
            SAW_INDEX = new Random().nextInt(3);

            trashHandler.add(SAW_INDEX);

            timer -= Settings.SPAWN_TIME;
        }

        trashHandler.draw(SAW_INDEX);
        trashHandler.setObjectListener(new ObjectListener() {
            @Override
            public void onObjectCollision(boolean reduce) {
                lives -= reduce ? 1 : 0;
                liveHandler.updateLives(lives);

                score += reduce ? 0 : 1;
                scoreHandler.updateScore(score);

                if (lives <= 0) {
                    gameOver();
                }

                Gdx.app.debug("PlayScene", String.valueOf(score));
            }

            @Override
            public void onObjectPassed() {
                lives -= 1;

                if (lives <= 0) {
                    gameOver();
                }
            }
        });

        game.batch.end();
    }

    private void gameOver() {
        int highscore = Settings.pref.getInteger("highscore", 0);

        if (score > highscore) {
            Settings.pref.putInteger("highscore", score);
            Settings.pref.flush();

            highscore = score;
        }

        playServices.submitScore(score);
        adsServices.showInterstitial();

        checkAchievement();
        Score scoreObject = new Score(score, highscore);
        game.setScreen(new OverScene(game, scoreObject, playServices, adsServices));
    }

    private void checkAchievement() {
        for (int i = 0; i < threshold.length; i++) {
            if (score >= threshold[i]) {
                playServices.unlockAchievement(i);
            }
        }
    }

    @Override
    public void render(float delta) {
        draw();
    }

    @Override
    public void pause() {
        super.pause();
    }
}

package com.rizki.trash.Scene;

import com.rizki.trash.GameMain;
import com.rizki.trash.Interface.AdsServices;
import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.Object.Score;
import com.rizki.trash.pref.Assets;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class OverScene extends ScreenAdapter {

    private GameMain game;
    private Score scoreObject;
    private PlayServices playServices;
    private AdsServices adsServices;

    private OrthographicCamera guiCam;
    private Vector3 touchPoint;

    private Rectangle homeBounds;
    private Rectangle restartBounds;
    private Rectangle rankBounds;

    private String scoreText;
    private String highText;

    public OverScene(GameMain game, Score scoreObject, PlayServices playServices,
                     AdsServices adsServices) {
        this.game = game;
        this.scoreObject = scoreObject;
        this.playServices = playServices;
        this.adsServices = adsServices;

        guiCam = new OrthographicCamera(game.width, game.height);
        guiCam.position.set(game.width / 2, game.height / 2, 0);

        homeBounds = new Rectangle(game.getCenter(Assets.home) - 280,
                220, Assets.home.getWidth(), Assets.home.getHeight());
        restartBounds = new Rectangle(game.getCenter(Assets.restart),
                220, Assets.restart.getWidth(), Assets.restart.getHeight());
        rankBounds = new Rectangle(game.getCenter(Assets.rank) + 280,
                220, Assets.rank.getWidth(), Assets.rank.getHeight());

        touchPoint = new Vector3();
    }

    private void draw() {
        Gdx.gl.glClearColor(((float) 0/255), ((float) 0/255), ((float) 0/255), 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        guiCam.update();
        game.batch.setProjectionMatrix(guiCam.combined);

        game.batch.begin();
        game.batch.draw(Assets.overBackground,game.getCenter(Assets.overBackground),0);
        game.batch.draw(Assets.gameOver, game.getCenter(Assets.gameOver),
                game.height - Assets.gameOver.getHeight() - 220);

        showHighscore();

        game.batch.draw(Assets.home,
                game.getCenter(Assets.home) - 280, 220);
        game.batch.draw(Assets.restart,
                game.getCenter(Assets.restart), 220);
        game.batch.draw(Assets.rank,
                game.getCenter(Assets.rank) + 280, 220);

        game.batch.end();
    }

    private void update() {
        if (Gdx.input.justTouched()) {
            guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

            if (homeBounds.contains(touchPoint.x, touchPoint.y)) {
                game.setScreen(new MenuScene(game, playServices, adsServices));
            }
            else if (restartBounds.contains(touchPoint.x, touchPoint.y)) {
                game.setScreen(new PlayScene(game, playServices, adsServices));
            }
            else if (rankBounds.contains(touchPoint.x, touchPoint.y)) {
                playServices.showLeaderboard();
            }
        }
    }

    private void showHighscore() {
        GlyphLayout glyphScore = new GlyphLayout();
        scoreText = "Score " + scoreObject.getScore();
        glyphScore.setText(Assets.gameFont, scoreText);
        Assets.gameFont.draw(game.batch, scoreText,
                Gdx.graphics.getWidth() / 2 - glyphScore.width / 2,
                Gdx.graphics.getHeight() / 2 + 100);

        GlyphLayout glyphHigh = new GlyphLayout();
        highText = "Best " + scoreObject.getHighScore();
        glyphHigh.setText(Assets.gameFont, highText);
        Assets.gameFont.draw(game.batch, highText,
                Gdx.graphics.getWidth() / 2 - glyphHigh.width / 2,
                Gdx.graphics.getHeight() / 2 - 100);
    }

    @Override
    public void render(float delta) {
        update();
        draw();
    }

    @Override
    public void pause() {
        super.pause();
    }
}

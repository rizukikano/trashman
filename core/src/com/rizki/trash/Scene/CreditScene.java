package com.rizki.trash.Scene;
import com.rizki.trash.GameMain;
import com.rizki.trash.Interface.AdsServices;
import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.pref.Assets;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
public class CreditScene extends ScreenAdapter {

    private PlayServices playServices;
    private AdsServices adsServices;
    private GameMain game;
    private OrthographicCamera guiCam;
    private Vector3 touchPoint;

    private Rectangle backBound;

    public CreditScene(GameMain game, final PlayServices playServices, AdsServices adsServices ){
        this.game = game;
        this.playServices = playServices;
        this.adsServices = adsServices;
        guiCam = new OrthographicCamera(game.width, game.height);
        guiCam.position.set(game.width / 2, game.height / 2, 0);
        backBound= new Rectangle(game.getCenter(Assets.back),
                250,Assets.back.getWidth(),Assets.back.getHeight());
        touchPoint = new Vector3();


    }
    private void draw(){
        Gdx.gl.glClearColor(((float) 83/255), ((float) 104/255), ((float) 211/255), 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        guiCam.update();
        game.batch.setProjectionMatrix(guiCam.combined);

        game.batch.begin();
        game.batch.draw(Assets.creditBackground,game.getCenter(Assets.creditBackground),0);
        game.batch.draw(Assets.back, game.getCenter(Assets.back),250);
        game.batch.end();
    }
    private void update() {
        if (Gdx.input.justTouched()) {
            guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));


            if (backBound.contains(touchPoint.x, touchPoint.y)) {
                game.setScreen(new MenuScene(game , playServices , adsServices));
            }
        }
    }
    @Override
    public void render(float delta) {
        update();
        draw();
    }

    @Override
    public void pause() {
        super.pause();
    }
}

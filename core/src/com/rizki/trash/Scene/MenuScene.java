package com.rizki.trash.Scene;

import com.rizki.trash.GameMain;
import com.rizki.trash.Interface.AdsServices;
import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.pref.Assets;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class MenuScene extends ScreenAdapter {

    private GameMain game;
    private PlayServices playServices;
    private AdsServices adsServices;

    private OrthographicCamera guiCam;
    private Vector3 touchPoint;

    private Rectangle achievBounds;
    private Rectangle rankBounds;
    private Rectangle playBounds;
    private Rectangle creditBounds;
    private Rectangle exitBounds;

    public MenuScene(GameMain game, final PlayServices playServices, AdsServices adsServices) {
        this.game = game;
        this.playServices = playServices;
        this.adsServices = adsServices;

        guiCam = new OrthographicCamera(game.width, game.height);
        guiCam.position.set(game.width / 2, game.height / 2, 0);

        achievBounds = new Rectangle(game.getCenter(Assets.achievement)-450,
                1700, Assets.achievement.getWidth(), Assets.achievement.getHeight());
        rankBounds = new Rectangle(game.getCenter(Assets.rank) + 450,
                1700, Assets.rank.getWidth(), Assets.rank.getHeight());
        playBounds = new Rectangle(game.getCenter(Assets.playButton),
                1000, Assets.playButton.getWidth(),Assets.playButton.getHeight());
        creditBounds = new Rectangle(game.getCenter(Assets.creditButton),
                700,Assets.creditButton.getWidth(),Assets.creditButton.getHeight());
        exitBounds = new Rectangle(game.getCenter(Assets.exitButton),
                400,Assets.exitButton.getWidth(),Assets.exitButton.getHeight());
        touchPoint = new Vector3();

        if (!this.playServices.isSignedIn()) {
            this.playServices.signIn();
        }
    }

    private void draw() {
        Gdx.gl.glClearColor(((float) 83/255), ((float) 104/255), ((float) 211/255), 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        guiCam.update();
        game.batch.setProjectionMatrix(guiCam.combined);

        game.batch.begin();

        game.batch.draw(Assets.mainBackground,game.getCenter(Assets.mainBackground),0);
        game.batch.draw(Assets.achievement,
                game.getCenter(Assets.achievement)-450, 1700);
        game.batch.draw(Assets.rank,
                game.getCenter(Assets.rank) + 450, 1700);
        game.batch.draw(Assets.playButton,game.getCenter(Assets.playButton),1000);
        game.batch.draw(Assets.exitButton,game.getCenter(Assets.exitButton),400);
        game.batch.draw(Assets.creditButton,game.getCenter(Assets.creditButton),700);


        game.batch.end();
    }

    private void update() {
        if (Gdx.input.justTouched()) {
            guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

            if (achievBounds.contains(touchPoint.x, touchPoint.y)) {
                if (playServices.isSignedIn()) {
                    playServices.showAchievement();
                }
            }
            else if (rankBounds.contains(touchPoint.x, touchPoint.y)) {
                if (playServices.isSignedIn()) {
                    playServices.showLeaderboard();
                }
            }
            else if (playBounds.contains(touchPoint.x,touchPoint.y)) {

                game.setScreen(new PlayScene(game, playServices, adsServices));
            } else if (creditBounds.contains(touchPoint.x,touchPoint.y)){
                game.setScreen(new CreditScene(game , playServices , adsServices));
            } else if(exitBounds.contains(touchPoint.x,touchPoint.y)){
                System.exit(0);
            }
        }

    }

    @Override
    public void render(float delta) {
        update();
        draw();
    }

    @Override
    public void pause() {
        super.pause();
    }
}

package com.rizki.trash.pref;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

import javax.xml.soap.Text;

public class Assets {

    public static BitmapFont gameFont;


    public static Texture gameBackground;

    public static Texture gameOver;
    public static Texture home;
    public static Texture rank;
    public static Texture achievement;

    public static Texture restart;
    public static Texture playButton;
    public static Texture exitButton;
    public static Texture creditButton;
    public static Texture overBackground;
    public static Texture mainBackground;
    public static Texture creditBackground;
    public static Texture back;

    public static Texture trashSprite;
    public static TextureRegion[] trash;


    public static Texture[] trashes;

    public static Music bgm;

    public static void load() {
        gameFont = getFont("fonts/Cute Cartoon.ttf");
        gameFont.setColor(Color.BLACK);


        gameOver = new Texture("over.png");
        home = new Texture("home.png");
        rank = new Texture("leaderboard.png");
        achievement = new Texture("achieve.png");
        restart = new Texture("restart.png");
        mainBackground = new Texture("BG.png");
        overBackground = new Texture("board.png");
        playButton = new Texture("play.png");
        exitButton = new Texture("exit.png");
        creditButton = new Texture("credit.png");
        gameBackground = new Texture("backgroundGame.png");
        creditBackground = new Texture("bgcredit.png");
        back = new Texture("back.png");



        trashSprite = new Texture("trash.png");
        trash = new TextureRegion[] {
                new TextureRegion(trashSprite, 0, 0, 297, 342),
        };


        trashes = new Texture[] {
                new Texture("apel.png"),
                new Texture("cola.png"),
                new Texture("poison.png")
        };

        bgm = Gdx.audio.newMusic(Gdx.files.internal("aloha.mp3"));
        bgm.setLooping(true);
        bgm.setVolume(0.5f);
        if (Settings.SOUND_ENABLED) bgm.play();
    }

    public static BitmapFont getFont(String path)
    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                Gdx.files.internal(path)
        );
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = (int) (Settings.FONT_SIZE * Gdx.graphics.getDensity());
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose();

        return font;
    }
}

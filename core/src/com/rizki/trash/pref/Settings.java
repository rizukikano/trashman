package com.rizki.trash.pref;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class Settings {
    public static boolean SOUND_ENABLED = true;
    public static int FONT_SIZE = 30;
    public static int VERTICAL_RANGE = 100;
    public static float MOVEMENT_SPEED = 2f;
    public static int FALLING_SPEED = -48;
    public static float SPAWN_TIME = 1.1f;

    public static final int RC_SIGN_IN = 9001;
    public static final int RC_ACHIEVEMENT_UI = 9003;
    public static final int RC_LEADERBOARD_UI = 9004;

    public static Preferences pref;

    public static void load() {
        pref = Gdx.app.getPreferences("preferences");
    }
}

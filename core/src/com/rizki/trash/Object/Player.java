package com.rizki.trash.Object;

import com.rizki.trash.parents.SpriteSheet;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Player extends SpriteSheet {
    public Player(float frameDuration, TextureRegion... keyFrames) {
        super(frameDuration, keyFrames);
    }
}

package com.rizki.trash.Object;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.rizki.trash.parents.SpriteSheet;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Trash extends Sprite {

    private int textureIndex = -1;
    private int posIndex = -1;
    private boolean reduce = false;

    public Trash(Texture[] textures, int textureIndex) {
        super(textures[textureIndex]);

        this.textureIndex = textureIndex;
        this.reduce = textureIndex == 2;
    }

    public int getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(int posIndex) {
        this.posIndex = posIndex;
    }

    public boolean isReduce() {
        return reduce;
    }

    public void setReduce(boolean reduce) {
        this.reduce = reduce;
    }
}

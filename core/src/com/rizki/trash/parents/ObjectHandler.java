package com.rizki.trash.parents;

import com.badlogic.gdx.graphics.Texture;
import com.rizki.trash.GameMain;
import com.rizki.trash.Interface.ObjectListener;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ObjectHandler {
    protected GameMain game;
    protected Sprite sprite;
    protected Texture[] textures;

    protected ObjectListener objectListener;

    public ObjectHandler(GameMain game, Sprite sprite, Texture... textures) {
        this.game = game;
        this.sprite = sprite;
        this.textures = textures;
    }

    protected void setObjectListener(ObjectListener objectListener) {
        this.objectListener = objectListener;
    }

    protected float getHorizPosition(int textureIndex, int index) {
        float sameWidth = (float) game.width / 3;
        float assetHalf = (float) textures[0].getWidth() * sprite.getScaleX() / 2;

        float side1 = (sameWidth / 2);
        float side2 = side1 + sameWidth;
        float side3 = side1 + sameWidth * 2;

        if (index == 0) {
            return side1 - assetHalf;
        }
        else if (index == 1) {
            return side2 - assetHalf;
        }
        else {
            return side3 - assetHalf;
        }
    }
}

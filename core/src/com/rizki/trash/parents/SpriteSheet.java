package com.rizki.trash.parents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteSheet extends Sprite {

    private TextureRegion textureRegion;
    private TextureRegion[] keyFrames;
    private float frameDuration;
    private float stateTime;

    public SpriteSheet(float frameDuration, TextureRegion... keyFrames) {
        this.frameDuration = frameDuration;
        this.keyFrames = keyFrames;

        stateTime = 0;
    }

    public void draw(Batch batch) {
        stateTime += Gdx.graphics.getDeltaTime();

        textureRegion = getKeyFrame(stateTime);

        batch.draw(textureRegion, this.getX(),
                this.getY(), textureRegion.getRegionWidth() * getScaleX(),
                textureRegion.getRegionHeight() * getScaleY());
        setBounds(this.getX(), this.getY(),
                textureRegion.getRegionWidth() * getScaleX(),
                textureRegion.getRegionHeight() * getScaleY());
    }

    public TextureRegion getKeyFrame(float stateTime) {
        int frameNumber = (int) (stateTime / frameDuration) % keyFrames.length;
        return keyFrames[frameNumber];
    }
}

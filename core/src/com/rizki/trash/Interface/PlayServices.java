package com.rizki.trash.Interface;

public interface PlayServices {
    public void signIn();
    public void signOut();
    public void silentSignIn();
    public boolean isSignedIn();
    public void showLeaderboard();
    public void submitScore(int score);
    public void showAchievement();
    public void unlockAchievement(int index);
}

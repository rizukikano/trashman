package com.rizki.trash.Interface;

public interface ObjectListener {
    public void onObjectCollision(boolean reduce);
    public void onObjectPassed();
}

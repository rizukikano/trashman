package com.rizki.trash;

import com.rizki.trash.Interface.AdsServices;
import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.Scene.MenuScene;
import com.rizki.trash.pref.Assets;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameMain extends Game {

	private PlayServices playServices;
	private AdsServices adsServices;

	public SpriteBatch batch;
	public int width;
	public int height;

	public GameMain(PlayServices playServices, AdsServices adsServices) {
		this.playServices = playServices;
		this.adsServices = adsServices;
	}

	@Override
	public void create() {
		batch = new SpriteBatch();

		Assets.load();
		Settings.load();

		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();

		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		setScreen(new MenuScene(this, playServices, adsServices));
	}

	@Override
	public void render() {
		super.render();
	}

	public float getCenter(Texture texture) {
		return ((float) width / 2) - ((float) texture.getWidth() / 2);
	}
}

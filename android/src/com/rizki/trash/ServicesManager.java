package com.rizki.trash;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.util.Log;

import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class ServicesManager implements PlayServices {

    private Context context;
    private GoogleSignInClient googleSignInClient;
    private LeaderboardsClient leaderboards;

    private GoogleSignInOptions options;
    private GoogleSignInAccount account;


    public ServicesManager(Context context) {
        this.context = context;

        options = GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN;
        account = GoogleSignIn.getLastSignedInAccount(context);

        googleSignInClient = GoogleSignIn.getClient(context,
                new GoogleSignInOptions.Builder(options).build());

        if (account == null) {
            signIn();
        }
        else {
            leaderboards = Games.getLeaderboardsClient(context, account);
        }
    }

    @Override
    public void signIn() {
        ((AndroidApplication) context).startActivityForResult(
                googleSignInClient.getSignInIntent(), Settings.RC_SIGN_IN);
    }

    @Override
    public void signOut() {
        final AndroidApplication application = ((AndroidApplication) context);

        googleSignInClient.signOut().addOnCompleteListener(application,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        boolean successful = task.isSuccessful();
                        Log.d("ServicesManager", "signOut(): " +
                                (successful ? "success" : "failed"));
                    }
                });
    }

    @Override
    public void silentSignIn() {
        googleSignInClient.silentSignIn().addOnCompleteListener(new OnCompleteListener<GoogleSignInAccount>() {
            @Override
            public void onComplete(@NonNull Task<GoogleSignInAccount> task) {

                try {
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    leaderboards = Games.getLeaderboardsClient(context, account);
                } catch (ApiException apiException) {
                    Gdx.app.debug("ServiceManager", apiException.getMessage());
                }
            }
        });
    }

    @Override
    public boolean isSignedIn() {
        account = GoogleSignIn.getLastSignedInAccount(context);
        return account != null;

    }

    @Override
    public void submitScore(int highscore) {
        AndroidApplication application = ((AndroidApplication) context);

        Games.getLeaderboardsClient(context, account)
                .submitScore(application.getString(R.string.leaderboard_id), highscore);
    }

    @Override
    public void showLeaderboard() {
        if (leaderboards == null) return;

        final AndroidApplication application = ((AndroidApplication) context);

        leaderboards.getLeaderboardIntent(application.getString(R.string.leaderboard_id))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        application.startActivityForResult(intent,
                                Settings.RC_LEADERBOARD_UI);
                    }
                });
    }

    @Override
    public void showAchievement() {
        if (account == null) return;

        final AndroidApplication application = ((AndroidApplication) context);

        Games.getAchievementsClient(context, account)
                .getAchievementsIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        application.startActivityForResult(intent,
                                Settings.RC_ACHIEVEMENT_UI);
                    }
                });
    }

    @Override
    public void unlockAchievement(int index) {
        if (account == null) return;

        AndroidApplication application = ((AndroidApplication) context);

        @SuppressLint("Recycle")
        TypedArray achiev = application.getResources().obtainTypedArray(R.array.achievements);

        Games.getAchievementsClient(context, account)
                .unlock(achiev.getString(index));
    }
}

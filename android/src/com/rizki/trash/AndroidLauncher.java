package com.rizki.trash;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.rizki.trash.Interface.AdsServices;
import com.rizki.trash.Interface.PlayServices;
import com.rizki.trash.pref.Settings;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class AndroidLauncher extends AndroidApplication {

	private InterstitialAd interstitialAd;

	private PlayServices playServices;
	private AdsServices adsServices;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;

		initServices();

		initialize(new GameMain(playServices, adsServices), config);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == Settings.RC_SIGN_IN) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);

			try {
				GoogleSignInAccount account = task.getResult(ApiException.class);
				playServices.silentSignIn();

				showToast("Sign In Success");
			} catch (ApiException apiException) {
				showToast(apiException.getMessage());
			}
		}

		if (requestCode == Settings.RC_ACHIEVEMENT_UI ||
				requestCode == Settings.RC_LEADERBOARD_UI) {

			// Check if User Signed out
			if (playServices != null && !playServices.isSignedIn()) {
				showToast("Sign Out Success");
				playServices.signIn();
			}
		}
	}

	void initServices() {
		playServices = new ServicesManager(this);

		MobileAds.initialize(this,
				getResources().getString(R.string.ads_id));

		interstitialAd = new InterstitialAd(this);
		interstitialAd.setAdUnitId(getResources().getString(R.string.inter_id));

		adsServices = new AdsManager(this, interstitialAd);
	}

	void showToast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

}

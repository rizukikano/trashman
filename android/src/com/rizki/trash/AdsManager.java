package com.rizki.trash;

import android.content.Context;

import com.rizki.trash.Interface.AdsServices;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AdsManager implements AdsServices {

    private Context context;
    private InterstitialAd interstitialAd;

    public AdsManager(Context context, InterstitialAd interstitialAd) {
        this.context = context;
        this.interstitialAd = interstitialAd;

        loadAds();

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loadAds();
            }
        });
    }

    @Override
    public void loadAds() {
        interstitialAd.loadAd(new AdRequest.Builder()
                .addTestDevice("C5FC454EB947A99ECE4BA5B862DA4238")
                .build());
    }

    @Override
    public void showInterstitial() {
        ((AndroidApplication) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }
            }
        });
    }
}
